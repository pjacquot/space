class('Ship').extends(Entity)

local image = playdate.graphics.image.new("sprites/canon.pdi")

function Ship:init(rect, speed, alive)
	Ship.super.init(self, rect, speed, alive)
	self.shots = {}
	for i = 1, 5 do
		r = playdate.geometry.rect.new(0, 0, 4, 16)
		self.shots[i] = Entity(r, 8, false)
	end
	self.sprite = playdate.graphics.sprite.new(image)
	self.sprite:add()
	self:align_sprite_on_rect()
end

function Ship:move(dx, dy)
	self.rect.x += dx * speed
	self.rect.y += dy * speed
	self:align_sprite_on_rect()
	self:move_shots()
end

function Ship:move_shots()
	for i, shot in ipairs(self.shots) do
		if shot.alive and shot.rect.y < - shot.rect.height then
			shot.alive = false
		elseif shot.alive then
			shot:move(0, -1)
		end
	end
end

function Ship:trigger_shot()
	for i, shot in ipairs(self.shots) do
		if shot.alive == false then
			local new_x = self.rect.x + self.rect.width / 2 - shot.rect.width / 2
			local new_y = self.rect.y - (shot.rect.height + 2)
			shot:set_position(new_x, new_y)
			shot.alive = true
			break
		end
	end
end

function Ship:shots_collide_with_placeholder_ennemies(placeholder)
	for i, shot in ipairs(self.shots) do
		if shot.alive then
			ennemy_collider = placeholder:collide_with(shot)
			if not (ennemy_collider == nil) then
				shot.alive = false
			end
		end
	end
end

function Ship:draw()
	-- playdate.graphics.fillRect(self.rect)
	for i, shot in ipairs(self.shots) do
		shot:draw()
	end
end

function Ship:reset(xpos, ypos)
	self.alive = true
	self:set_position(xpos, ypos)
	for i, shot in ipairs(self.shots) do
		shot.alive = false
	end
end

function Ship:align_sprite_on_rect()
	self.sprite:moveTo(self.rect.x + self.rect.width/2, self.rect.y + self.rect.height/2)
end
