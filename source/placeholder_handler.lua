import "placeholder"

class('PlaceholderHandler').extends()

local x_start_pos = 32
local y_start_pos = 20

local ennemy_width = 32
local ennemy_height = ennemy_width

local ennemy_starts_alive = true
local ennemy_speed = 1

local padding = 8

local ennemies_number = 8

local ennemy_catchup_distance = ennemy_width

function PlaceholderHandler:init(placeholders_number)
	self.placeholders_number = placeholders_number
	self.placeholders_list = {}
	self.respawn_y_pos = y_start_pos
	self.target_y_pos_list = {}
	for i = 1, self.placeholders_number do
		self:add_placeholder(i)
	end
	self.horizontal_movement = 1
end

function PlaceholderHandler:add_placeholder(i)
	local rect = playdate.geometry.rect.new(x_start_pos, self.respawn_y_pos, ennemy_width, ennemy_height)
	local placeholder = Placeholder(rect, ennemy_speed, ennemy_starts_alive, padding)
	for i = 1, ennemies_number do
		placeholder:add_ennemy()
	end
	self.placeholders_list[i] = placeholder
	self.target_y_pos_list[i] = self.respawn_y_pos
	self.respawn_y_pos = y_start_pos - i*ennemy_height
end

function PlaceholderHandler:move_placeholders()
	local vertical_movement = 0
	local tmp_rect = self.placeholders_list[1].rect
	if tmp_rect.x + tmp_rect.width >= 400 then
		self.horizontal_movement = -1
		vertical_movement = 8
		self.respawn_y_pos += 8
	elseif tmp_rect.x <= 0 then
		self.horizontal_movement = 1
		vertical_movement = 8
		self.respawn_y_pos += 8
	end
	for i, placeholder in ipairs(self.placeholders_list) do
		local vertical_catchup = 0
		if placeholder:is_everybody_dead() then
			placeholder:reset(placeholder.rect.x, self.respawn_y_pos - ennemy_catchup_distance)
			self.target_y_pos_list[i] = self.respawn_y_pos
			self.respawn_y_pos = self.respawn_y_pos - ennemy_height
			increment_score(line_clear_reward)
		elseif placeholder.rect.y < self.target_y_pos_list[i] then
			vertical_catchup = 1
		end
		placeholder:move(self.horizontal_movement, vertical_movement + vertical_catchup)
	end
end

function PlaceholderHandler:ship_shots_collide_with_ennemies(ship)
	for i, placeholder in ipairs(self.placeholders_list) do
		ship:shots_collide_with_placeholder_ennemies(placeholder)
	end
end

function PlaceholderHandler:ennemies_shots_collide_with(ship)
	collide = false
	for i, placeholder in ipairs(self.placeholders_list) do
		if placeholder:shots_collide_with(ship) then
			collide = true
			break
		end
	end
	return collide
end

function PlaceholderHandler:draw_placeholders()
	for i, placeholder in ipairs(self.placeholders_list) do
		placeholder:draw()
	end
end

function PlaceholderHandler:reset()
	for i, placeholder in ipairs(self.placeholders_list) do
		local y_pos = y_start_pos - (i-1)*ennemy_height
		placeholder:reset(x_start_pos, y_pos)
		self.target_y_pos_list[i] = y_pos
	end
	self.respawn_y_pos = y_start_pos - self.placeholders_number*ennemy_height
end

