function set_title_screen()
	playdate.stop()
	title_screen = true
	playdate.graphics.drawText("*Space (invaders)*", 130, 110)
	playdate.graphics.drawText("*Press 'A' to begin !*", 250, 220)
	playdate.display.flush()
end

function set_gameover_screen()
	local score_text = string.format("*Final score: %dpts*", score)
	playdate.stop()
	gameover_screen = true
	playdate.graphics.clear()
	playdate.graphics.drawText(score_text, 40, 80)
	playdate.graphics.drawText("*Game over.*", 40, 110)
	playdate.graphics.drawText("*Press 'A' to start new game*", 180, 220)
	playdate.display.flush()
end

