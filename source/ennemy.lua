class('Ennemy').extends(Entity)

local image = playdate.graphics.image.new("sprites/octo.pdi")

function Ennemy:init(rect, speed, alive)
	Ennemy.super.init(self, rect, speed, alive)
	local shot_rect = playdate.geometry.rect.new(0, 0, 4, 16)
	self.shot = Entity(shot_rect, 4, false)
	self.sprite = playdate.graphics.sprite.new(image)
	self:align_sprite_on_rect()

	-- this line might look like a duplicate of Ennemy.super.init call but
	-- it is for adding or removing sprite from display list regarding
	-- the value of alive
	self:set_alive(alive)
end

function Ennemy:trigger_shot()
	if self.alive and self.shot.alive == false then
		local dice = math.random(100)
		if dice == 100 then
			self.shot.rect.x = self.rect.x + self.rect.width / 2 + 1
			self.shot.rect.y = self.rect.y + self.rect.height + 2
			self.shot.alive = true
		end
	end
end

function Ennemy:move(dx, dy)
	self.rect.x += dx * self.speed
	self.rect.y += dy * self.speed
	self:align_sprite_on_rect()
	if self.shot.alive then
		if self.shot.rect.y > 400 then
			self.shot.alive = false
		end
		self.shot:move(0, 1)
	end
end

function Ennemy:draw()
	self.shot:draw()
end

-- other: Entity instance
function Ennemy:collide_with(other)
	local collision = self.rect:intersects(other.rect)
	if collision then
		self:set_alive(false)
		self.sprite:remove()
		increment_score(ennemy_defeat_reward)
	end
	return collision
end

function Ennemy:align_sprite_on_rect()
	self.sprite:moveTo(self.rect.x + self.rect.width/2, self.rect.y + self.rect.height/2)
end

function Ennemy:set_alive(alive)
	self.alive = alive
	if self.alive then
		self.sprite:add()
	else
		self.sprite:remove()
	end
end
