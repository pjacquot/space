class('Placeholder').extends(Entity)

function Placeholder:init(rect, speed, alive, padding)
	Placeholder.super.init(self, rect, speed, alive)
	self.ennemies = {}
	self.ennemies_number = 0
	self.base_width = self.rect.width
	self.base_height = self.rect.height
	self.padding = padding
end

function Placeholder:add_ennemy()
	local x = self.rect.x + self.padding + self.ennemies_number * (self.base_width + self.padding)
	local y = self.rect.y
	local rect = playdate.geometry.rect.new(x, y, self.base_width, self.base_height)
	local e = Ennemy(rect, self.speed, self.alive)
	-- In lua, for loops won't iterate starting from 0, so we must start to 1.
	self.ennemies_number += 1
	self.ennemies[self.ennemies_number] = e
	self.rect.width = self.padding + self.ennemies_number * (self.base_width + self.padding)
end

-- other: Entity instance
function Placeholder:collide_with(other)
	local collider = nil
	for i, ennemy in ipairs(self.ennemies) do
		if ennemy.alive and ennemy:collide_with(other) then
			collider = ennemy
			break
		end
	end
	return collider
end

-- player must be an entity
function Placeholder:shots_collide_with(player)
	local collide = false
	for i, ennemy in ipairs(self.ennemies) do
		if ennemy.shot.alive and ennemy.shot:collide_with(player) then
			collide = true
			ennemy.shot.alive = false
			break
		end
	end
	return collide
end

function Placeholder:move(dx, dy)
	self.rect.x += dx * self.speed
	self.rect.y += dy * self.speed
	self:move_ennemies(dx, dy)
end

function Placeholder:move_ennemies(dx, dy)
	for i, ennemy in ipairs(self.ennemies) do
		ennemy:move(dx, dy)
		ennemy:trigger_shot()
	end
end

function Placeholder:draw()
	if self.alive then
		-- playdate.graphics.drawRect(self.rect)
		for i, ennemy in ipairs(self.ennemies) do
			ennemy:draw()
		end
	end
end

function Placeholder:reset(xpos, ypos)
	self:set_position(xpos, ypos)
	for i, ennemy in ipairs(self.ennemies) do
		local x = self.rect.x + self.padding + (i - 1) * (self.base_width + self.padding)
		local y = self.rect.y
		ennemy:set_position(x, y)
		ennemy:align_sprite_on_rect()
		ennemy:set_alive(true)
		ennemy.shot.alive = false
	end
end

function Placeholder:is_everybody_dead()
	local everybody_dead = true
	for i, ennemy in ipairs(self.ennemies) do
		if ennemy.alive then
			everybody_dead = false
			break
		end
	end
	return everybody_dead
end
