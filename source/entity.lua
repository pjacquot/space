class('Entity').extends()

function Entity:init(rect, speed, alive)
	self.rect = rect
	self.speed = speed
	self.alive = alive
end

function Entity:move(dx, dy)
	self.rect.x += dx * self.speed
	self.rect.y += dy * self.speed
end

function Entity:set_position(xpos, ypos)
	self.rect.x = xpos
	self.rect.y = ypos
end

-- other: Entity instance
function Entity:collide_with(other)
	local collision = self.rect:intersects(other.rect)
	if collision then
		self.alive = false
	end
	return collision
end

function Entity:draw()
	if self.alive then
		playdate.graphics.fillRect(self.rect)
	end
end

