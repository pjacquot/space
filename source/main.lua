import "CoreLibs/object"
import "CoreLibs/graphics"
import "CoreLibs/sprites"
import "CoreLibs/timer"

import "entity"
import "ennemy"
import "placeholder_handler"
import "ship"
import "util"

math.randomseed(playdate.getSecondsSinceEpoch())

-- Player
xpos = 190
ypos = 220
width = 32
height = 16
rect = playdate.geometry.rect.new(xpos, ypos, width, height)
speed = 4
local player = Ship(rect, speed, true)


handler = PlaceholderHandler(3)

title_screen = true
gameover_screen = false

playdate.display.setInverted(true)

set_title_screen()

score = 0
ennemy_defeat_reward = 10
line_clear_reward = 100

function playdate.AButtonUp()
	if title_screen then
		title_screen = false
		playdate.start()
	elseif gameover_screen then
		gameover_screen = false
		reset()
		playdate.start()
	end
end

function playdate.update()
	playdate.graphics.clear()
	playdate.graphics.sprite.update()
	player_x_movement = 0
	if playdate.buttonIsPressed(playdate.kButtonLeft) then
		player_x_movement = -1
	end
	if playdate.buttonIsPressed(playdate.kButtonRight) then
		player_x_movement = 1
	end

	player:move(player_x_movement, 0)
	handler:move_placeholders()
	handler:ship_shots_collide_with_ennemies(player)
	if handler:ennemies_shots_collide_with(player) then
		set_gameover_screen()
		return
	end

	if playdate.buttonJustReleased(playdate.kButtonB) then
		player:trigger_shot()
	end
	display_entities()
end

function display_entities()
	player:draw()
	handler:draw_placeholders()
	draw_score()
end

function reset()
	handler:reset()
	player:reset(xpos, ypos)
	score = 0
end

function increment_score(points)
	score += points
end

function draw_score()
	score_text = string.format("*%dpts*", score)
	playdate.graphics.drawText(score_text, 20, 220)
end
