PDC=${PLAYDATE_SDK_PATH}/bin/pdc

BUILD=space.pdx

${BUILD}: source/main.lua source/entity.lua source/ennemy.lua source/placeholder.lua source/placeholder_handler.lua source/ship.lua source/util.lua
	${PDC} source ${BUILD}

clean:
	rm -rf ${BUILD} 
